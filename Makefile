include /usr/share/dpkg/pkg-info.mk

PACKAGE=proxmox-backup

DEB=$(PACKAGE)_$(DEB_VERSION)_all.deb
DSC = $(PACKAGE)_$(DEB_VERSION).dsc

DEBS=$(DEB)

BUILD_DIR ?= $(DEB_SOURCE)-$(DEB_VERSION)

all:

$(BUILD_DIR):
	rm -rf $@ $@.tmp
	mkdir -p $@.tmp/debian
	cp -ar debian/* $@.tmp/debian/
	echo "git clone git://git.proxmox.com/git/proxmox-backup-meta.git\\ngit checkout $$(git rev-parse HEAD)" >$@.tmp/debian/SOURCE
	mv $@.tmp $@

.PHONY: deb
deb: $(DEB)
$(DEB): $(BUILD_DIR)
	cd $(BUILD_DIR); dpkg-buildpackage -b -uc -us
	lintian $(DEBS)

.PHONY: dsc
dsc:
	$(MAKE) clean
	$(MAKE) $(DSC)
	lintian $(DSC)

$(DSC): $(BUILD_DIR)
	cd $(BUILD_DIR); dpkg-buildpackage -S -uc -us -d

sbuild: $(DSC)
	sbuild $<

.PHONY: upload
upload: UPLOAD_DIST ?= $(DEB_DISTRIBUTION)
upload: $(DEBS)
	tar cf - $(DEBS)|ssh -X repoman@repo.proxmox.com -- upload --product pbs --dist $(UPLOAD_DIST)

.PHONY: clean
clean:
	rm -rf -- $(PACKAGE)-[0-9]*/
	rm -f $(PACKAGE)*.tar* *.deb *.buildinfo *.changes *.build *.dsc
